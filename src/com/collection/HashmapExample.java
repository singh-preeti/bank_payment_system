package com.collection;

import java.util.HashMap;
 
class SmallArms {
	String Name;
	String CountryOfOrigin;
	public SmallArms(String name, String countryOfOrigin) {
		super();
		Name = name;
		CountryOfOrigin = countryOfOrigin;
	}
 
	@Override
	public String toString() {
		return "SmallArms [Name=" + Name + ", CountryOfOrigin=" + CountryOfOrigin + "]";
	}	

}
 
public class HashmapExample {
 
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		HashMap<Integer, SmallArms> SmallArmMap = new HashMap<>();
		SmallArmMap.put(1, new SmallArms("AK-74","SOVIET UNION"));
		SmallArmMap.put(2, new SmallArms("AK-203","RUSSIAN FEDERATION"));
		SmallArmMap.put(3, new SmallArms("TAVOR","ISRAEL"));
		SmallArmMap.put(5, new SmallArms("MP5","GERMANY"));
		SmallArmMap.put(6, new SmallArms("FNFAN","BELGIUM"));
		System.out.println("SmallArmMap > " + SmallArmMap);
		System.out.println("SmallArmMap > " + SmallArmMap.size());
		System.out.println("SmallArmMap > " + SmallArmMap.putIfAbsent(8, new SmallArms("GALIL", "ISRAEL")));
		System.out.println("SmallArmMap > " + SmallArmMap.size());		
		System.out.println("SmallArmMap > " + SmallArmMap.remove(5, new SmallArms("MP5","GERMANY")));
		System.out.println("SmallArmMap ocoks> " + SmallArmMap.size());
		System.out.println("SmallArmMap > " + SmallArmMap);
		System.out.println("SmallArmMap > " + SmallArmMap.containsValue(new SmallArms("MP5","GERMANY")));
		System.out.println("SmallArmMap > " + SmallArmMap.keySet());
		System.out.println("SmallArmMap > " + SmallArmMap.entrySet());