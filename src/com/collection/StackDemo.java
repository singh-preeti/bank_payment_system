package com.collection;

import java.util.Stack;

public class StackDemo {
	public static void main(String[] args) {
		Stack<String> names = new Stack<>();
		names.push("Preety");
		names.push("Sachin");
		names.push("Meghana");
		names.push("Ravi");
		names.push("Roshan");
		names.push("Sagarika");
		
		System.out.println(names);
		
		names.pop();
		
		System.out.println(names);
		
	}

}
